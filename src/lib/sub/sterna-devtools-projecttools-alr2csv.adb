--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 20yy STERNA MARINE s.a.s (william.franck@Sterna.io)
--  SPDX-Creator: name_of_author (name_of_author@domain.ext)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-01-10
--  --------------------------------------------------------------------------------------

package body Sterna.DevTools.ProjectTools.Alr2CSV is

   -- ----------
   --  Read_TOML
   -- ----------
   procedure Read_TOML (F : Text_IO.File_Type)
   is
   begin
      null;
   end Read_TOML;

   -- ----------
   --  Write_CSV
   -- ----------
   procedure Write_CSV (F : Text_IO.File_Type)
   is
   begin
      null;
   end Write_CSV;

end Sterna.DevTools.ProjectTools.Alr2CSV;
