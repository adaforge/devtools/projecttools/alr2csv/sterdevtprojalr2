pragma Wide_Character_Encoding (UTF8);
--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 20yy STERNA MARINE s.a.s (william.franck@Sterna.io)
--  SPDX-Creator: name_of_author (name_of_author@domain.ext)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-01-10
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Ada.Text_IO;
use Ada;

package Sterna.DevTools.ProjectTools.Alr2CSV is

   procedure Read_TOML (F : Text_IO.File_Type);
   procedure Write_CSV (F : Text_IO.File_Type);

end Sterna.DevTools.ProjectTools.Alr2CSV;
