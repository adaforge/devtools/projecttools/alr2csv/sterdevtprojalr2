--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 20yy STERNA MARINE s.a.s (william.franck@Sterna.io)
--  SPDX-Creator: name_of_author (name_of_author@domain.ext)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2023-01-10
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Alr2CSV.Process_Files;
use Alr2CSV.Process_Files;

--  with CLIC_Ex.Commands;
with Command_Line;
use Command_Line;

with Ada.Directories;
use Ada.Directories;
use Ada;

procedure Sterna_DevTools_ProjectTools_Alr2CSV
is
   Arg_File_Kind : Directories.File_Kind;
   --  Run_Args : Args_array;

begin
   --  --------------
   --      MAIN
   --  --------------
   --  get the command line arguments
   --  CLIC_Ex.Commands.Execute;
   Command_Line.Get_Args; -- (Objects => Run_Args);
   Arg_File_Kind := Directories.Kind (OS_File_Name.To_String (Get_Arg (DirEntry_Name)));

   case Arg_File_Kind is
      when Directory =>
         case Switch (Command_Line.Index).Value is
            when True =>
               Process_Index (OS_File_Name.To_String (Get_Arg (DirEntry_Name)));
            when False =>
               Directories.Search (
                  Directory => OS_File_Name.To_String (Get_Arg (DirEntry_Name)),
                  Pattern =>  "",
                  Filter  => (Directory => True, others => False),
                  Process => Process_Crate'Access);
         end case;

      when Ordinary_File =>
         Process_TOML (OS_File_Name.To_String (Get_Arg (DirEntry_Name)));

      when others =>
         raise Directories.Name_Error;
   end case;

exception
   when HELP_REQUIRED =>
      null; -- quit silently

end Sterna_DevTools_ProjectTools_Alr2CSV;
