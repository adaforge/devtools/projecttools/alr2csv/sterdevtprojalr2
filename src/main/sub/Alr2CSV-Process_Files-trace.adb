separate (Alr2CSV.Process_Files)
package body Trace is
   procedure Advice (CrossPoint : JoinPoint) is

   --  From Sterna_Devtools_Projecttools_Alr2csv_Conf;
   --     type Log_Level_Kind is (Info, Debug, Warn, Error);
   --     Log_Level : constant Log_Level_Kind := ???;  -- see 'alire.toml'

   begin
      if Switch (Command_Line.Trace).Value then -- Trace
         case CrossPoint is

            when Show_File_Name =>
               Text_IO.Put_Line (OS_File_Name.To_String (Info.File_Name));

            when Show_Process =>
               if  Log_Level in (Debug) then
                  Text_IO.Put_Line ("   ...");
               end if;

            --  when others =>
            --     null;
         end case;
      end if;
   end Advice;
end Trace;