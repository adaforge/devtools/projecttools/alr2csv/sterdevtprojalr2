with Sterna.DevTools.ProjectTools.Alr2CSV;
use Sterna.DevTools.ProjectTools;

with GNAT.Directory_Operations;
with GNAT.Directory_Operations.Iteration;
use GNAT.Directory_Operations;

with Command_Line;
use Command_Line;

with Sterna_Devtools_Projecttools_Alr2csv_Config;
use Sterna_Devtools_Projecttools_Alr2csv_Config;

package body Alr2CSV.Process_Files is

   --  --------------
   --  Trace «Aspect»
   --  --------------
   package Trace is
      type JoinPoint is (Show_File_Name, Show_Process);
      type Trace_Info is record
         File_Name : OS_File_Name.Bounded_String := OS_File_Name.Null_Bounded_String;
      end record;
      Info : Trace_Info;
      procedure Advice (CrossPoint : JoinPoint);
   end Trace;
   package body Trace is separate;
   use Trace;

   --  -------------
   --  Process_Index
   --  -------------
   procedure Process_Index (Directory_Entry : String)
   is
      Search_Pattern : constant Path_Name := Directory_Entry & "/index/*/*/*.toml";

      procedure Action
           (Item  :        String;
            Dummy_Index :        Positive;
            Quit  : in out Boolean) is
      begin
         Trace.Info.File_Name := OS_File_Name.To_Bounded_String (Item);
         Trace.Advice (Show_File_Name); -- Trace


         Process_TOML (Item);

         Quit := False;
      end Action;

      procedure Wildcard_Iterator is 
         new Iteration.Wildcard_Iterator (Action);
   begin
      Wildcard_Iterator (Search_Pattern);
   end Process_Index;

   --  -------------
   --  Process_Crate
   --  -------------
   procedure Process_Crate (Directory_Entry : Directory_Entry_Type)
   is
   begin
      Directories.Search (
               Directory => Directories.Full_Name (Directory_Entry),
               Pattern =>  TOML_File_Name,
               Filter  => (Ordinary_File => True, others => False),
               Process => Process_ALIRE'Access);
   end Process_Crate;

   --  -------------
   --  Process_ALIRE
   --  -------------
   procedure Process_ALIRE (Directory_Entry : Directory_Entry_Type)
   is
      Full_TOML_File_Name : constant String
            := Directories.Full_Name (Directory_Entry);
   begin
            Trace.Info.File_Name := OS_File_Name.To_Bounded_String (Full_TOML_File_Name);
            Trace.Advice (Show_File_Name); -- Trace

            Process_TOML (Full_TOML_File_Name);

   end Process_ALIRE;

   --  -------------
   --  Process_TOML
   --  -------------
   procedure Process_TOML (File_Name : String)
   is
      TOML_File : Text_IO.File_Type;
   begin
      Text_IO.Open (TOML_File, Text_IO.In_File, File_Name);

      Trace.Advice (Show_Process); -- Trace

      Sterna.DevTools.ProjectTools.Alr2CSV.Read_TOML (TOML_File);
      Sterna.DevTools.ProjectTools.Alr2CSV.Write_CSV (CSV_File);
      Text_IO.Close (TOML_File);
   end Process_TOML;

end Alr2CSV.Process_Files;
