with Ada.Directories;
use Ada.Directories;

with Ada.Text_IO;
use Ada;

package Alr2CSV.Process_Files is
--   pragma Preelaborate;
   TOML_File_Name : constant String := "*.toml";
   CSV_File : constant Text_IO.File_Type := Text_IO.Standard_Output;

   procedure Process_Index (Directory_Entry : String);
   procedure Process_Crate (Directory_Entry : Directory_Entry_Type);
   procedure Process_ALIRE (Directory_Entry : Directory_Entry_Type);
   procedure Process_TOML (File_Name : String);

private
end Alr2CSV.Process_Files;
