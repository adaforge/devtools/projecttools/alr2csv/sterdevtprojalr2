--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2020 STERNA MARINE s.a.s. (william.franck@Sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@Sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------

with Ada.Command_Line;

with Ada.Text_IO;
with Ada.Wide_Wide_Text_IO;

use Ada;

package body Command_Line is

   type Args_array is array (Defined_Arg) of OS_File_Name.Bounded_String;
   Args : Args_array := (others => OS_File_Name.Null_Bounded_String);

   type Optional_Switch is access all Defined_Switch;
   Actual_Switch : aliased Defined_Switch;
   Latest_Switch : Optional_Switch := null;

   --  type Optional_Switch is access all Switch_Object;
   --  type Switch_array is array (Defined_Switch) of Switch_Info;
   --  Switches : Switch_array := (
   --     Help  => new Help_Switch,   -- (Help => "Lists the parameters that can be used"),
   --     Trace => new Trace_Switch,  -- (Help => "Trace of intermediate results"),
   --     Index => new Index_Switch); -- (Help => "Search through a ALIRE index file structure")

   type Switch_array is array (Defined_Switch) of Switch_Object;

   Switches : Switch_array := (
      Help  => (
            ID => 'h',
            Name => To_Bounded_String ("help"),
            Help => To_Bounded_Wide_Wide_String ("Lists the parameters that can be used"),
            Value         => False,
            Default_Value => False),
      Trace => (
            ID => 't',
            Name => To_Bounded_String ("trace"),
            Help => To_Bounded_Wide_Wide_String ("Trace of intermediate results"),
            Value         => False,
            Default_Value => False),
      Index => (
            ID => 'i',
            Name => To_Bounded_String ("index"),
            Help => To_Bounded_Wide_Wide_String ("Search through a ALIRE index file structure"),
            Value         => False,
            Default_Value => False)
   );

   --  --------------
   --  Get_Arg
   --  --------------
   function Get_Arg (Arg_Name : Defined_Arg) return OS_File_Name.Bounded_String
   is
   begin
      return Args (Arg_Name);
   end Get_Arg;

   --  --------------
   --  Switch
   --  --------------
   function Switch (Switch_Name : Defined_Switch) return Switch_Object
   is
   begin
      return Switches (Switch_Name);
   end Switch;

   --  --------------
   --  Process_Switch
   --  --------------
   function Process_Switch (Arg_Switch : String) return Defined_Switch
   is
   begin
      if Arg_Switch'Length = 1 then
         for S in Defined_Switch loop
            if Switches (S).ID = Arg_Switch (Arg_Switch'First) then
               Switches (S).Value := True;
               return S;
            end if;
         end loop;
         raise BAD_ARGUMENTS;
      elsif Arg_Switch'Length > 1 then
         for S in Defined_Switch loop
            if Switches (S).Name = Arg_Switch
            then
               Switches (S).Value := True;
               return S;
            end if;
         end loop;
         raise BAD_ARGUMENTS;
      else
         raise BAD_ARGUMENTS;
      end if;
   end Process_Switch;

   --  ---------------
   --  Check_Arg_Index
   --  ---------------
   function Check_Arg_Index (
         Index  : Defined_Arg;
         Switch : Optional_Switch)
         return Defined_Arg
   is
   begin
      if Switch = null then
         return Index;
      else  -- in case of specific argument tied to a switch
         return Index;
      end if;
   end Check_Arg_Index;

   --  --------
   --  Get_Args
   --  --------
   procedure Get_Args -- (Objects : Program_Args)
   is

      package ACL renames Ada.Command_Line;

      Args_Count : Natural := 0;
      Arg_Index : Defined_Arg := Defined_Arg'First;

   begin
      for i in 1 .. ACL.Argument_Count loop

         if ACL.Argument (i)'Length = 2
         and then ACL.Argument (i)(1) = '-'
         then
            Actual_Switch := Process_Switch (ACL.Argument (i)(2 .. 2));
            Latest_Switch := Actual_Switch'Access;
         elsif ACL.Argument (i)'Length > 2
         and then ACL.Argument (i)(1 .. 2) = "--"
         then
            Actual_Switch := Process_Switch (ACL.Argument (i)(3 .. ACL.Argument (i)'Length));
            Latest_Switch := Actual_Switch'Access;
         else
            Arg_Index := Check_Arg_Index (Arg_Index, Latest_Switch);

            Args (Arg_Index) := To_Bounded_String (ACL.Argument (i));
            Args_Count := @ + 1;

            if Arg_Index /= Defined_Arg'Last then
               Arg_Index := Defined_Arg'Succ (Arg_Index);
            end if;
         end if;
      end loop;

      if Switches (Help).Value then
         raise HELP_REQUIRED;
      end if;

      if Args_Count = 0 then
         raise MISSING_FILE_NAME;
      end if;

   exception
      when MISSING_FILE_NAME =>
         Text_IO.Put_Line (Text_IO.Standard_Error, "Missing file or directory name ...");
         raise;

      when BAD_ARGUMENTS | HELP_REQUIRED =>
         for i in Error_Message'Range loop
            Wide_Wide_Text_IO.Put (
                  File => Wide_Wide_Text_IO.Standard_Error,
                  Item => Console_String.To_Wide_Wide_String (Error_Message (i)));
         end loop;
         ACL.Set_Exit_Status (ACL.Failure);
         raise;
   end Get_Args;

end Command_Line;
