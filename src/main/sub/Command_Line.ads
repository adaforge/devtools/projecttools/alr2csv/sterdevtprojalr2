--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: Apache-2.0
--  SPDX-FileCopyrightText: Copyright 2020 STERNA MARINE s.a.s. (william.franck@Sterna.io)
--  SPDX-Creator: William J. FRANCK (william.franck@Sterna.io)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2020-01-10
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

pragma Wide_Character_Encoding (UTF8);

with Ada.Strings.Wide_Wide_Bounded;
with Ada.Strings.Bounded;
with Ada.Characters.Latin_1;
with Ada.Characters.Conversions;
use Ada.Characters.Conversions;
use Ada.Characters;

package Command_Line is

--   pragma Preelaborate;

   package OS_File_Name is new Ada.Strings.Bounded.Generic_Bounded_Length (Max => 1_024);
   use OS_File_Name;

   package Switch_Names is new Ada.Strings.Bounded.Generic_Bounded_Length (Max => 16);
   use Switch_Names;

   package Console_String is new Ada.Strings.Wide_Wide_Bounded.Generic_Bounded_Length (Max => 132);
   subtype WWS is Console_String.Bounded_Wide_Wide_String;
   use Console_String;

   --  Specific Arguments----------------------------
   type Defined_Arg is (DirEntry_Name, Out_File_Name);

   function Get_Arg (Arg_Name : Defined_Arg) return OS_File_Name.Bounded_String;

   --  ----------------------------------------------
   type Switch_Object is -- tagged
   record
      ID            : Character;
      Name          : Switch_Names.Bounded_String;
      Value         : Boolean := False;
      Default_Value : Boolean := False;
      Help          : WWS;
   end record;

   type Defined_Switch is (Help, Trace, Index);

--   type Optional_Switch is access all Switch;

   --  Specific switches  ---------------------------
--   type Help_Switch is new Switch_Object with null record;
   --     ID   : Character := 'h';
   --     Name : String (1 .. 4) := "help";
   --  end record;

--   type Trace_Switch is new Switch_Object with null record;
   --     ID    : Character := 't';
   --     Name  : String (1 .. 5) := "trace";
   --  end record;

--   type Index_Switch is new Switch_Object with null record;
   --     ID    : Character := 'i';
   --     Name  : String (1 .. 5) := "index";
   --  end record;

   function Switch (Switch_Name : Defined_Switch) return Switch_Object;

   --  ----------------------------------------------

   type Console_Text_array is array (Positive range <>) of Console_String.Bounded_Wide_Wide_String;

   Error_Message : Console_Text_array := (
            To_Bounded_Wide_Wide_String (
                  "Usage : "
                  & To_Wide_Wide_Character (Latin_1.LF)
                  & "        ACL.Command_Name"
                  & " [ -t ] [[ -i ] directory_name | ALIRE_TOML_file_name ]] "),
            To_Bounded_Wide_Wide_String (
                  To_Wide_Wide_Character (Latin_1.LF)
                  & "Options:"),
            To_Bounded_Wide_Wide_String (
                  To_Wide_Wide_Character (Latin_1.LF)
                  & "        -t --trace   : Trace of intermediate results"),
            To_Bounded_Wide_Wide_String (
                  To_Wide_Wide_Character (Latin_1.LF)
                  & "        -i --index   : Search through a ALIRE index file structure")
   );

   BAD_ARGUMENTS,
   HELP_REQUIRED,
   MISSING_FILE_NAME : exception;

   procedure Get_Args; -- (Objects : Args_array); --; Switches : in out Switch_array);

end Command_Line;
