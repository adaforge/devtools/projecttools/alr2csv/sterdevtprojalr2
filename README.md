# Purpose

Convert "alire.toml" info file to a CSV file.

Arguments on calling this tool may be a single file, or a directory containing Alire projects.

## Fields read and converted

From Alire `Labels` definitions in specification `Alire.Properties.Labeled`

* Author : VIP
* Description : Free-form but short description
* Executable : A resulting executable built by the release
* Hint : A text to display to the user for externals that fail detection
* Long_Description : Unlimited-length crate description
* Maintainer : Info about the maintainer of the alr-packaged crate
* Maintainers_Logins : E-mails used by the maintainers of a crate to log in to GitHub
* Name : Crate name
* Notes : Specific information about a release
* Path : Extra path for PATH to add to build (prepended)
* Project_File : Buildable project files in the release, with full relative path
* Version : Semantic version of the release
* Website : A website other than the repository
* Tag : One word that identify one of the topic convered by a crate

# Usage

```shell
Usage : bin/sterdevtbuilalir [ -t | -T ] [ directory_name | ALIRE_TOML_file_name ] 
Options:
	-t --trace   : Trace of intermediate results
	-T --timings : Trace of intermediate results
```

# Results - Output

All `alire.toml` files read and processed are concactenated into a single CSV output stream.

First line represents the list of attributes (label) names.

CSV separator is character `,` and each field is delimited by character `"` and each line is ended by `ASCII.LF`.

This tool outputs to the `Standard_Output`.